Oceanset Map Building Advice

Overworld Maps

- I recommend doing ground details last. The optimal method is to cover the entire map with _one_ plain green grass tile. Next, add in the dark grass (see note below), but don't apply the borders until you've done _all_ the dark grass. You can quickly place the dark grass tiles _over_ any tiles you want them to be behind. Then select all the dark grass at once and apply the borders together (perhaps right click -> change pattern of similar tiles, then undo. All similar tiles will still be selected). Then, while the dark grass and borders are still selected, you can press "B" to send them to the back, behind all other tiles. Then press "B" on the single grass tile to send it behind the dark grass, and the borders and dark grass will now be behind all your trees, cliffs, walls, etc.

- The dark grass works best in corners and especially around trees. Oceanset doesn't have shadows for trees, but loosely surrounding clusters of trees with dark grass helps the trees seem more anchored into a map.

- When using the grass blade details, don't just scatter them randomly. Use them in clumps of 2-4, and weight them more heavily toward edges of "rooms" as walled in by trees and cliffs.

- Additionally, don't be too heavy with these grass details. The foreground elements in this tileset are designed to be visually busier than the grounds, so making the ground too busy or too high contrast will work against the foreground elements like trees and buildings.

- Use light grass details more sparingly than dark grass details, as they stand out more. Use flowers even more sparingly, as they stand out the most.

- Many tiles that default to layer 1 have a fairly large transparency around them. Watch out for this when placing smaller ground details around them so the ground details (leaves, grass) don't accidentally end up on layer 1 instead of layer 0. I find it helpful to keep my finger on the "1" key, turning that layer off as I place each element, then turning it back on so I can see the whole context as I'm placing.

- _Oceanset_ cliffs are like the gameboy Zelda games- they're very flexible and don't need many special-case tiles. However, if a east/west clifftop edge runs into the corner of a cliff-face, there is a special tile for this, the cliff-face corners with the wide black bottoms.

- The other notable cliff tiles, are two sets of interior corners. One is pixeled fully to the edge, while the other has transparent pixels. The one pixeled fully to the edge is useful for covering up other tiles, which is intended to happen frequently, due to following note.

- _Oceanset_ cliffs are 32px (2 tiles) wide instead of the standard 16px (one tile). Same of the sides and top edges. If you want a cliff to run an odd number of tiles long, overlap the last 16px with a corner instead of placing the corner adjacent to the edge. Overlapping of tiles is intended. Of course, the best way to deal with this is to make your cliffs run an even number of tiles in length.

- For parts of cliffs where you want the hero to be able to jump down, the stanard I use is "cutting" a section of the cliff-top-walls (I call them crusts), then placing 8px of the ground type with the corresponding ground-edge.

- For places where you want the hero to be able to jump off a cliff sideways, there isn't necessarily as good a solution, but I recommend something similar as jumping down. Or getting creative and making a jumping-off platform from planks or something.

- The cliffs have no diagonal tiles, so loosely zig-zagging will have to do. This does, however, allow for the illusion of sloping ground, as with each zig zag the height of the cliff can increase or decrease.

- If making a forest, there are some mushrooms in the _Oceanset Inside_ tileset that might work well for decoration.

- For the wooden planks, if used as bridges, I find that although a middle tile is provided so they may scale to any width, the top and bottom tiles make the best bridge- at 2-tile width the proportions work best.

- Most grounds, if used as a pathway (for instance, the various stone tiles) should be 2 tiles wide at most points, as you'll usually use the "Inside Grass Edges" to imply grass growing around the edges of the path. The exception to this is the dirt, which has its own edges, which make the path wider. In this case, a 1 tile wide path looks fine.
