--[[
	This unit test for the multi_events script tests an event returning true to end the
	event chain.
	
	Final sequences are the following, where () denotes mt or non-reg events:
	obj1 = "obj1b;();obj1a;obj1c"
	mt1 = "mt1b;();mt1a;mt1c"
	mt_mt1 = "mt_mt1c;mt_mt1a;(mt_mt_nonreg);mt_mt1b"
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

local multi_events = require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

function map:on_opening_transition_finished()
	local return_id
	
	local mt_mt1 = {}
	mt_mt1.__index = mt_mt1
	multi_events:enable(mt_mt1)
	
	local mt1 = {}
	mt1.__index = mt1
	setmetatable(mt1, mt_mt1)
	multi_events:enable(mt1)
	
	local obj1 = setmetatable({}, mt1)
	multi_events:enable(obj1)
	local function obj1_trigger() obj1:on_activated() end
	events_proto:set_trigger(obj1_trigger)
	
	
	--Test #1: register some events to obj1
	obj1:register_event("on_activated", function()
		events_proto:log"obj1a"
		return return_id=="obj1a"
	end)
	obj1:register_event("on_activated", function()
		events_proto:log"obj1b"
		return return_id=="obj1b" or nil
	end, true)
	obj1:register_event("on_activated", function()
		events_proto:log"obj1c"
		if return_id=="obj1c" then return true end
	end)
	return_id = "obj1a"
	events_proto:trigger"obj1b;obj1a" --skips: obj1c
	
	
	--Test #2: register some meta meta events
	function mt_mt1:on_activated()
		events_proto:log"mt_mt_nonreg"
		return return_id=="mt_mt_nonreg"
	end
	mt_mt1:register_event("on_activated", function()
		events_proto:log"mt_mt1a"
		return return_id=="mt_mt1a"
	end, true)
	mt_mt1:register_event("on_activated", function()
		events_proto:log"mt_mt1b"
		return return_id=="mt_mt1b"
	end)
	mt_mt1:register_event("on_activated", function()
		events_proto:log"mt_mt1c"
		return return_id=="mt_mt1c"
	end, true)
	return_id = "mt_mt1a"
	events_proto:trigger"obj1b;mt_mt1c;mt_mt1a" --skips: mt_mt_nonreg , mt_mt1b , obj1a , obj1c
	
	
	--Test #3: register some meta events
	mt1:register_event("on_activated", function()
		events_proto:log"mt1a"
		return return_id=="mt1a"
	end)
	mt1:register_event("on_activated", function()
		events_proto:log"mt1b"
		return return_id=="mt1b"
	end, true)
	mt1:register_event("on_activated", function()
		events_proto:log"mt1c"
		return return_id=="mt1c"
	end)
	return_id = nil
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a;mt_mt_nonreg;mt_mt1b;mt1a;mt1c;obj1a;obj1c"
	
	
	--Test #4: individually return true for each event in chain
	return_id = "obj1b"
	events_proto:trigger"obj1b"
	return_id = "mt1b"
	events_proto:trigger"obj1b;mt1b"
	return_id = "mt_mt1c"
	events_proto:trigger"obj1b;mt1b;mt_mt1c"
	return_id = "mt_mt1a"
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a"
	return_id = "mt_mt_nonreg"
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a;mt_mt_nonreg"
	return_id = "mt_mt1b"
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a;mt_mt_nonreg;mt_mt1b"
	return_id = "mt1a"
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a;mt_mt_nonreg;mt_mt1b;mt1a"
	return_id = "mt1c"
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a;mt_mt_nonreg;mt_mt1b;mt1a;mt1c"
	return_id = "obj1a"
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a;mt_mt_nonreg;mt_mt1b;mt1a;mt1c;obj1a"
	return_id = "obj1c"
	events_proto:trigger"obj1b;mt1b;mt_mt1c;mt_mt1a;mt_mt_nonreg;mt_mt1b;mt1a;mt1c;obj1a;obj1c"
	
	
	events_proto:exit()
end
