local map = ...
local game = map:get_game()

local function get_enemies()
  local enemies = {}
  for enemy in map:get_entities_by_type("enemy") do
    enemies[#enemies + 1] = enemy
  end
  return enemies
end

function map:on_started()
  game:set_max_life(12)
  game:set_life(game:get_max_life())
  game:set_ability("sword", 1)
end

function map:on_opening_transition_finished()

  local was_pushed_by_wind = false
  local was_hurt = false

  function hero:on_position_changed()
    if was_pushed_by_wind then
      return
    end
    local x, y = hero:get_position()
    local initial_x, initial_y = destination:get_position()
    if x ~= initial_x or y ~= initial_y then
      -- We should have been pushed by wind.
      local state = hero:get_state()
      assert(state == "free")
      was_pushed_by_wind = true
    end
  end

  function hero:on_state_changed(state)
    if state == "hurt" then
      assert(was_pushed_by_wind)
      local enemies = get_enemies()
      assert(#enemies == 2)
      assert(enemies[1]:get_breed() == "slime_purple")
      assert(enemies[2]:get_breed() == "slime_egg")
      was_hurt = true
      -- Hit the enemy.
      sol.timer.start(map, 1000, function()
        -- The egg was turned into a purple slime.
        enemies = get_enemies()
        assert(#enemies == 2)
        assert(enemies[1]:get_breed() == "slime_purple")
        assert(enemies[2]:get_breed() == "slime_purple")
        game:simulate_command_pressed("attack")
        sol.timer.start(map, 1000, function()
           -- The purple slime fell and was replaced by a geen slime with a purple sprite.
          enemies = get_enemies()
          assert(#enemies == 2)
          assert(enemies[1]:get_breed() == "slime_purple")
          assert(enemies[2]:get_breed() == "slime_green")
          assert(enemies[2]:get_sprite():get_animation_set() == "enemies/slime_purple")
          sol.main.exit()
        end)
      end)
    end
  end
end
