local map = ...
local game = map:get_game()

function map:on_started()
  game:set_max_life(12)
  game:set_life(game:get_max_life())
  game:set_ability("sword", 1)
end

function map:on_opening_transition_finished()

  sol.timer.start(map, 20000, sol.main.exit)
end
